﻿using hw_interfaces.Entities;

namespace hw_interfaces
{
    public static class Predicates<T>
    {
        public static bool SelectItemAboutMe(T item)
        {
            if (typeof(T) == typeof(Account))
            {
                Account accItem = item as Account;
                return accItem.FirstName.ToLower() == "шибанов" && accItem.LastName.ToLower() == "сергей";
            }
            else
                return false;
        }

        public static bool SelectItem(T item)
        {
            if (typeof(T) == typeof(Account))
            {
                Account accItem = item as Account;
                return accItem.FirstName.ToLower() == "Пупкин";
            }
            else
                return false;
        }

    }
}
