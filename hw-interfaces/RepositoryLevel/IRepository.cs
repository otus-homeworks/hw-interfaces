﻿using hw_interfaces.Entities;
using System;
using System.Collections.Generic;

namespace hw_interfaces.RepositoryLevel
{
    public interface IRepository<T>
    {
        // когда реализуете этот метод, используйте yield (его можно использовать просто в методе, без создания отдельного класса)
        IEnumerable<T> GetAll();
        //T GetOne( Func<T, bool> predicate );
        T GetOne(Func<Account, bool> predicate);
        void Add(T item);
    }
}
