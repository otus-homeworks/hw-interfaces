﻿using System.Collections.Generic;
using System.Linq;

namespace hw_interfaces
{
   public interface ISorted<T>
   {
      IEnumerable<T> Sort(IEnumerable<T> notSortedItems);
   }

   class PersonSorter<T> : ISorted<T>
   {
      public IEnumerable<T> Sort(IEnumerable<T> notSortedItems)
      {
         var result = notSortedItems.OrderBy(s => s);
         return result;

         //var res = notSortedItems.ToList();
         //res.Sort();
         //return res;
      }
   }
}
