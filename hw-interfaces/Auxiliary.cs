﻿using hw_interfaces.Entities;
using System;
using System.Collections.Generic;
using System.IO;

namespace hw_interfaces
{
    public static class Auxiliary
    {
        public static void ShowForPersons()
        {
            ClearPepository();
            MakeFileWithRecordsOfPersons();

            var persons = new List<Person>();

            if (File.Exists(Constants.NameOfFileWithObjs))
            {
                using StreamReader sr = new StreamReader(Constants.NameOfFileWithObjs);
                var otusStreamRd = new OtusStreamReader<Person>(sr, new OtusXmlSerializer<Person>());

                foreach (Person prs in otusStreamRd)
                    persons.Add(prs);
            }

            ShowListOfPersons(persons, "В исходном порядке");

            var personSorter = new PersonSorter<Person>();

            var sortedPersons = personSorter.Sort(persons);

            ShowListOfPersons(sortedPersons, "После сортировки");
        }

        // при смене параметра T между Person и Account
        public static void ClearPepository()
        {
            if (File.Exists(Constants.NameOfFileWithObjs))
                File.Delete(Constants.NameOfFileWithObjs);
        }

        public static int MakeFileWithRecordsOfPersons()
        {
            // Запись в файл NameOfFileWithObjs данных списка persons. 
            // В каждой строке файла хранится сериализованая в xml строка, соотв. одной персоне

            var persons = new List<Person>()
            {
                new Person( "Иванов", 20),
                new Person( "Петров", 30),
                new Person( "Сидоров", 40),
                new Person( "Шибанов", 43),
                new Person( "Васечкин", 33),
                new Person( "Иванова", 22)
            };

            var otusXmlSerializer = new OtusXmlSerializer<Person>();

            using StreamWriter wr = new StreamWriter(Constants.NameOfFileWithObjs, false);

            foreach (Person person in persons)
            {
                wr.WriteLine(otusXmlSerializer.Serialize(person));
                wr.WriteLine(Constants.ObjDelimiterInFile);
            }

            return persons.Count;
        }

        public static void ShowListOfPersons(IEnumerable<Person> persons, string Caption)
        {
            Console.WriteLine(Caption);
            foreach (Person prs in persons)
            {
                Console.WriteLine(prs);
            }
            Console.WriteLine();
        }
    }
}
