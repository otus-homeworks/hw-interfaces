﻿using System;

namespace hw_interfaces.Entities
{
    [Serializable]
    public class Person : IComparable
    {
        public string Name { get; set; }
        public int Age { get; set; }

        // стандартный конструктор без параметров (для штатного сериализатора).
        public Person()
        { }

        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public override string ToString()
        {
            return $"  Имя : {Name,12}     Возраст {Age,4}";
        }

        public int CompareTo(object obj)
        {
            if (obj is Person tmp)
            {
                if (Age > tmp.Age)
                    return 1;
                if (Age < tmp.Age)
                    return -1;

                return 0;
            }
            else
                throw new Exception("В Person.CompareTo передан obj не являющийся объектом Person");
        }
    }
}
