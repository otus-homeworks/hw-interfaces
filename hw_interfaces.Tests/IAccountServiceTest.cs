using hw_interfaces.Entities;
using hw_interfaces.RepositoryLevel;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xunit;

namespace hw_interfaces.Test
{
    public class IAccountServiceTest
    {
        [Fact]
        public void AddAccount_ValidAccount_Added()
        {
            // Arrange
            Auxiliary.ClearPepository();
            var repo = new MyRepository<Account>();
            IAccountService accountService = new AccountService(repo);
            accountService.AddAccount(new Account("�������", "������", new DateTime(1977, 10, 27)));

            // Act
            var result = repo.GetOne(Predicates<Account>.SelectItemAboutMe);

            // Assert
            Assert.NotNull(accountService);
            Assert.NotNull(result);
            Assert.Equal("�������", result.FirstName);
            Assert.Equal("������", result.LastName);
        }

        [Fact]
        public void AddAccount_InvalidAccount_NotAdded()
        {
            // Arrange
            Auxiliary.ClearPepository();
            var repo = new MyRepository<Account>();
            IAccountService accountService = new AccountService(repo);

            // Act
            accountService.AddAccount(new Account("�������", "����", new DateTime(2009, 5, 7)));
            var result = repo.GetAll();

            // Assert
            Assert.NotNull(accountService);
            Assert.NotNull(result);
            Assert.Empty(result);
        }

        [Fact]
        public void GetAll_AddThreeAccounts_ThreeAccountsAdded()
        {
            // Arrange
            Auxiliary.ClearPepository();
            var repo = new MyRepository<Account>();
            IAccountService accountService = new AccountService(repo);
            accountService.AddAccount(new Account("�������", "������", new DateTime(1977, 10, 27)));
            accountService.AddAccount(new Account("������", "��������", new DateTime(2000, 7, 19)));
            accountService.AddAccount(new Account("������", "����", new DateTime(2001, 7, 21)));

            // Act
            var result = repo.GetAll();

            // Assert
            Assert.NotNull(accountService);
            Assert.NotNull(result);
            Assert.Equal(3, result.ToList().Count);
            Assert.Equal("������", result.ToList()[2].FirstName);
        }

        [Fact]
        public void AddAccount_UseMockAddValid_repoAddCalled()
        {
            // Arrange
            var mockRepo = new Mock<IRepository<Account>>();
            var accountService = new AccountService(mockRepo.Object);

            // Act
            var testAccount = new Account("�������", "������", new DateTime(1977, 10, 27));
            accountService.AddAccount(testAccount);

            // Assert
            mockRepo.Verify(r => r.Add(testAccount), Times.Once);
        }

        [Fact]
        public void AddAccount_UseMockAddInValid_peroAddNotCall()
        {
            // Arrange        
            var mockRepo = new Mock<IRepository<Account>>();
            var accountService = new AccountService(mockRepo.Object);

            // Act
            var testAccount = new Account("�������", "����", new DateTime(2009, 5, 7));
            accountService.AddAccount(testAccount);

            // Assert
            mockRepo.Verify(r => r.Add(testAccount), Times.Never);
        }


        [Fact]
        public void OtusStreamReaderGetEnumerator_ReadElementsTwice_ListsEqual()
        {
            // Arrange
            Auxiliary.ClearPepository();
            int personsNumInFile = Auxiliary.MakeFileWithRecordsOfPersons();

            using StreamReader sr = new StreamReader(Constants.NameOfFileWithObjs);
            var otusStreamRd = new OtusStreamReader<Person>(sr, new OtusXmlSerializer<Person>());

            var personsListFirst = new List<Person>();
            var personsListSecond = new List<Person>();

            // Act
            foreach (Person prs in otusStreamRd)
                personsListFirst.Add(prs);
            
            foreach (Person prs in otusStreamRd)
                personsListSecond.Add(prs);

            // Assert            
            Assert.Equal(personsNumInFile, personsListSecond.Count);
            Assert.Equal(personsListFirst.Count, personsListSecond.Count);
            for (int i = 0; i < personsListFirst.Count; i++)
            {
                var prsInFirstList = personsListFirst.ElementAt(i);
                var prsInSecondList = personsListSecond.ElementAt(i);

                Assert.Equal(prsInFirstList.Age, prsInSecondList.Age);
                Assert.Equal(prsInFirstList.Name, prsInSecondList.Name);
            }
        }

        [Fact]
        public void OtusStreamReaderGetEnumerator_FirstAnIncompleteReading_ReadedAllElements()
        {
            // Arrange
            Auxiliary.ClearPepository();
            int personsNumInFile = Auxiliary.MakeFileWithRecordsOfPersons();

            using StreamReader sr = new StreamReader(Constants.NameOfFileWithObjs);
            var otusStreamRd = new OtusStreamReader<Person>(sr, new OtusXmlSerializer<Person>());

            var personsList = new List<Person>();

            // Act
            foreach (Person prs in otusStreamRd)
                if (prs.Age > 32)
                    break;

            foreach (Person prs in otusStreamRd)
                personsList.Add(prs);

            // Assert            
            Assert.Equal(personsNumInFile, personsList.Count);
        }
    }
}
